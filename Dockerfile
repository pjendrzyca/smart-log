FROM node:8

# create app directory
WORKDIR /user/src/app

# copy and install dependecies
COPY package*.json ./

RUN npm install

COPY . .

RUN npm link

