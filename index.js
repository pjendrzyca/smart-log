const program = require("commander");
const logParser = require("./lib/logParser");
const dateParser = require("./lib/dateParser");
const gitEmail = require("./lib/gitEmail");
const {populateTestClasses} = require('./lib/testClassFinder');

program
    .version("4.2.0 - BLAZE IT")
    .option("-n, --count <count>", "Number of commits you want to parse")
    .option("-u, --user <email>", "Git email address. If none provided the value will be taken from git global config")
    .option("-d, --since <date>", "Valid options: TODAY, YESTERDAY, WEEK, MONTH, ALL")
    .option("-p, --path <path>", "Path to git repository, if none provided cwd will be used instead")
    .option("-t, --includeTests", "Indicates if test classes should be included")
    .option("--all", "Parses logs for all users")
    .parse(process.argv);


async function run() {
    // if all flag set, dont specify user
    // otherwise check if user was specified
    // if not take the username from git settings
    const author = program.all ? null : program.user || await gitEmail()

    if(program.includeTests){
        await populateTestClasses();
    }

    const options = {
        author,
        repo: program.path || process.cwd(),
        includeTests: program.includeTests,
        number: program.count || 100,
        fields: ["authorEmail", "authorName", "authorDate", "subject", "body"],
        since: dateParser(program.since),
        execOptions: {
            maxBuffer: 15000 * 1024
        }
    };
    logParser(options);
}

run();
