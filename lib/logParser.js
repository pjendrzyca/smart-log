const moment = require('moment');
const gitlog = require("gitlog");
const {findTest} = require("./testClassFinder");

moment.locale('en');

const METADATA_TYPES = {
    'cls': 'ApexClass',
    'component': 'ApexComponent',
    'page': 'ApexPage',
    'email': 'Email',
    'resource': 'StaticResource',
    'aura': 'AuraDefinitionBundle',
    'trigger': 'ApexTrigger',
    'lwc' : 'LightningComponentBundle'
};

const parseFilePath = ([file, status]) => {
    const paths = file.split("/");
    const fileWithExtension = [...paths].pop();
    let [name, extension] = fileWithExtension.split(".");
    

    const isAura = paths.includes('aura');
    const isResource = paths.includes('staticresources');
    const isResourceBundle = paths.includes('resource-bundles');
    const isLwc = paths.includes('lwc');

    name = isAura ? paths[paths.length - 2] : name;

    const type = isAura ? 'aura' : isResource ? 'resource' : isLwc ? 'lwc' : extension;

    const metadata = METADATA_TYPES[type];

    // static resource and have extension - should be filtered out
    const exclude = (extension && isResourceBundle) || (extension || '').endsWith('-meta');

    return {
        name,
        extension,
        metadata,
        exclude,
        status,
        isAura,
    };
};

const parseSubject = subject => {
    const re = /^[a-zA-z]+\-\d+/;
    const matched = subject.match(re);

    return {
        task: !!matched ? matched[0] : "N/A",
        message: !!matched ? subject.substring(matched[0].length).trim() : subject
    };
};

const zip = (a, b) => {
    return a.map((el, idx) => {
        return [el, b[idx]]
    })
}

const formatCommits = commits => {
    const parsed = commits.map(commit => {
        const seenFiles = {};

        const files = zip(commit.files, commit.status)
            .map(parseFilePath)
            .filter(file => !file.exclude)
            .filter(file => {
              if(file.isAura){
                return seenFiles.hasOwnProperty(file.name) ? false : (seenFiles[file.name] = true);
              }
              return true;
            });
        
        return {
            date: moment(new Date(commit.authorDate)).format('l'),
            author: commit.authorName,
            email: commit.authorEmail,
            initials: commit.authorEmail.slice(0, 2).toUpperCase(),
            files,
            ...parseSubject(commit.subject)
        };
    });
    return parsed;
};

const logReader = options => {
    return new Promise((resolve, reject) => {
        gitlog(options, (error, commits) => {
            if (error) {
                reject(error);
            } else {
                resolve(commits);
            }
        });
    });
};

async function read(options) {
    return await logReader(options)
        .then(formatCommits)
        .catch(err => console.log("err", err));
}

async function parseCommitsToCsv(options) {
    const parsed = await read(options);

    const csv = parsed
        .reverse()
        .reduce((acc, commit) => {
            const entries = commit.files.map(file => {
                const message = file.status === 'D' ? 'DEPRACATED ' : commit.message

                let test = '';
                if(options.includeTests){
                    test = file.metadata === 'ApexClass' && !file.name.toLowerCase().endsWith('test') ?
                        findTest(file.name, .2) : '';
                }

                return [
                    file.name,
                    file.metadata,
                    test,
                    commit.date,
                    commit.task,
                    '',
                    commit.initials,
                    message,
                    'dev'
                ].join('\t');
            });

            return [acc, ...entries].join('\n');
        }, '').substring(1);

    console.log(csv);
    return csv;
}

module.exports = parseCommitsToCsv;
