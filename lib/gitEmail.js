const util = require("util");
const exec = util.promisify(require("child_process").exec);

module.exports = async function() {
  const { stdout, stderr } = await exec("git config user.email");
  return (stdout || '').trim();
}
