const moment = require('moment');

module.exports = function(literal) {
  switch (literal) {
    case 'TODAY': {
      return moment().hours(0).toDate();
    }
    case 'YESTERDAY': {
      return moment().days(-1).hours(0).toDate();
    }
    case 'WEEK': {
      return moment().days(-7).hours(0).toDate();
    }
    case 'MONTH': {
      return moment().month(-1).hours(0).toDate();
    }
    case 'ALL': {
      return moment(0).toDate()
    }
    default: {
      return moment(0).toDate()
    }

  }
}