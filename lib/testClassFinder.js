const path = require("path");
const util = require("util");
const fs = require("fs");
const ls = util.promisify(fs.readdir);
const levenshtein = require('js-levenshtein');

const classesDir = path.join(process.cwd(), 'src/classes');

const compare = (e1, e2) => {
    if (e1.distance === e2.distance) {
        return 0;
    }
    if (e1.distance < e2.distance) {
        return -1;
    } else return 1;
};

const normalizedLevenshtein = (expected, actual) => {
    return levenshtein(expected.toLowerCase(), actual.toLowerCase()) / expected.length;
};

const removeExtension = name => name.slice(0, -4);
let testClasses;

const populateTestClasses = async () => {
    testClasses = (await ls(classesDir))
        .filter(className => className.toLowerCase().endsWith('test.cls'))
        .map(removeExtension);
};

const find = (name, threshold) => {
    const expectedName = name + '_TEST';

    const candidates = testClasses
        .map((className) => {
            return {
                candidate: className,
                actual: name,
                distance: normalizedLevenshtein(expectedName, className),
            }
        })
        .filter(e => e.distance <= threshold)
        .sort(compare);

    const foundTest = candidates.shift();
    return !!foundTest ? foundTest.candidate : '';
};

module.exports = {
    findTest: find,
    populateTestClasses,
};
