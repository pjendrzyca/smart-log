## smart-log

#### Description
Simple CLI tool to ease working with changelogs


#### Installing
`nmp install -g`

#### Making accessible from command line
`nmp link` 

#### Usage
```
Options:
  -V, --version        output the version number
  -n, --count <count>  Number of commits you want to parse
  -u, --user <email>   Git email address. If none provided the value will be taken from git global config
  -d, --since <date>   Valid options: TODAY, YESTERDAY, WEEK, MONTH, ALL
  -p, --path <path>    Path to git repository, if none provided cwd will be used instead
  --all                Parses logs for all users
  -h, --help           output usage information
```

#### Example
`smart-log -d TODAY -u pjendrzyca@ohoho.pl`

#### Note
If you are UX/Mac user you can easly pipe the output to your clipboard
`smart-log -d TODAY | xclip selection -c`

For windows users
`smart-log -d TODAY | clip`

You can also add an alias to your git global config to copy all your changes to clipboard on push, like this
```
[alias]
  smartpush = ! git push -u origin master | smart-log -n 1 | xclip -selection c
```